#!/usr/bin/env bash

ARCHITECTURES="amd64 armv7hf aarch64"
ROOT_IMAGE="thibaultmorin/homecx"

PROJECT_DIR="$(pwd)"
BUILD_DIR="${PROJECT_DIR}/build"

DOCKER_VERSION=$(docker version -f "{{.Client.Version}}")

# docker architecture: amd64(x86_64) arm arm64(aarch64)
CURRENT_ARCHITECTURE=$(docker system info -f "{{.Architecture}}")
if [[ ${CURRENT_ARCHITECTURE} == "x86_64" ]]; then
    CURRENT_ARCHITECTURE="amd64"
elif [[ ${CURRENT_ARCHITECTURE} == "aarch64" ]]; then
    CURRENT_ARCHITECTURE="arm64"
fi

echo "PROJECT_DIR: $PROJECT_DIR"
echo "BUILD_DIR: $BUILD_DIR"
echo "DOCKER_VERSION: $DOCKER_VERSION"
echo "CURRENT_ARCHITECTURE: $CURRENT_ARCHITECTURE"

mkdir -p .docker
echo "{\"experimental\": \"enabled\"}" > .docker/config.json

if [[ ! -z "${CI_DOCKER_TOKEN}" ]]; then
    docker --config .docker login -u thibaultmorin -p ${CI_DOCKER_TOKEN}
fi

mkdir -p ${BUILD_DIR}

function toDockerArch {
    local architecture="${1}"
    case ${1} in
    armv7hf)
        architecture="arm"
        ;;
    aarch64)
        architecture="arm64"
        ;;
    esac
    echo ${architecture}
}

function toWebhookArch {
    local architecture=${1}
    case ${1} in
    amd64)
        architecture="386"
        ;;
    esac
    echo ${architecture}
}

function buildAndPublishImage {
    local architecture="$1"
    local docker_architecture=`toDockerArch ${architecture}`
    local version="$2"
    local version_minor="$3"
    local version_major="$4"
    local image="${ROOT_IMAGE}-${docker_architecture}"

    echo -e "\n## build and publishes the image [$image:$version] ##"

    local dockerfile="${BUILD_DIR}/Dockerfile.${docker_architecture}"

    cp -f ${PROJECT_DIR}/Dockerfile ${dockerfile}
    if [[ ${architecture} != ${CURRENT_ARCHITECTURE} ]]; then
        sed -i -e 's/#RUN \[ "cross-build-start" \]/RUN [ "cross-build-start" ]/g' ${dockerfile}
        sed -i -e 's/#RUN \[ "cross-build-end" \]/RUN [ "cross-build-end" ]/g' ${dockerfile}
    fi

    local image_tag="${image}:${version}"
    local image_minor_tag="${image}:${version_minor}"
    local image_major_tag="${image}:${version_major}"

    docker --config .docker pull -q ${image_tag} > /dev/null 2>&1
    local imageId=`docker --config .docker image ls -q ${image_tag}`
    if [[ -z ${imageId} ]]; then
        local webhook_architecture=`toWebhookArch ${docker_architecture}`
        echo -e "\n-- build docker image [${image_tag}] [${architecture}|${docker_architecture}|${webhook_architecture}]"
        docker --config .docker build -f ${dockerfile} \
            --build-arg ARCHITECTURE=${architecture} \
            --build-arg WEBHOOK_ARCHICTURE=${webhook_architecture} \
            -t ${image_tag} . \
        && docker --config .docker tag ${image_tag} ${image_minor_tag} \
        && docker --config .docker tag ${image_tag} ${image_major_tag} \
        && docker --config .docker push ${image_tag} \
        && docker --config .docker push ${image_minor_tag} \
        && docker --config .docker push ${image_major_tag}
        if [[ $? -ne 0 ]]; then
            exit 1
        fi
    else
        echo "build skipped"
    fi
}

function updateAndPublishManifest() {
    local manifest_list="$1"
    local version="$2"
    local architectures="$3"
    local manifest=""

    echo -e "\n## updates and publishes manifest [$manifest_list | $architectures] ##"

    for architecture in ${architectures}; do
        local docker_architecture=`toDockerArch ${architecture}`
        manifest="$manifest ${ROOT_IMAGE}-${docker_architecture}:${version}"
    done

    docker --config .docker manifest inspect ${manifest_list} > /dev/null 2>&1
    if [[ ! $? -eq 0 ]]; then
        echo "create manifest ${manifest_list} ${manifest}"
        docker --config .docker manifest create ${manifest_list} ${manifest}
    else
        echo "amend manifest ${manifest_list} ${manifest}"
        docker --config .docker manifest create --amend ${manifest_list} ${manifest}
    fi
    if [[ $? -ne 0 ]]; then
        echo "cannot create or amend"
        exit 2
    fi
    for architecture in ${architectures}; do
        local docker_architecture=`toDockerArch ${architecture}`
        docker --config .docker manifest annotate ${manifest_list} "${ROOT_IMAGE}-${docker_architecture}:${version}" --arch ${docker_architecture} --os linux
        if [[ $? -ne 0 ]]; then
            echo "cannot: docker --config .docker manifest annotate ${manifest_list} "${ROOT_IMAGE}-${docker_architecture}" --arch ${docker_architecture} --os linux"
            exit 3
        fi
    done
    docker --config .docker manifest push -p ${manifest_list}
    if [[ $? -ne 0 ]]; then
        echo "cannot: docker --config .docker manifest push -p ${manifest_list}"
        exit 4
    fi
}

function build {
    local gitBranch=$(git rev-parse --abbrev-ref HEAD)
    local branch=${CI_COMMIT_REF_NAME:-$gitBranch}
    branch=${branch//\//_}
    local version_full="${branch#tags/*}"
    local version_minor=$(echo ${version_full} | sed -E "s/([0-9]*\.[0-9]*)\.[0-9]*/\1/")
    local version_major=$(echo ${version_full} | sed -E "s/([0-9]*)\.[0-9]*\.[0-9]*/\1/")

    for architecture in ${ARCHITECTURES}; do
        buildAndPublishImage "${architecture}" "${version_full}" "${version_minor}" "${version_major}"
    done

    local versions=`echo -e "$version_full\n$version_minor\n$version_major" | sort -u | xargs`
    for version in ${versions}; do
        updateAndPublishManifest "${ROOT_IMAGE}:${version}" "${version}" "${ARCHITECTURES}"
    done
}

build
