ARG ARCHITECTURE="amd64"
# https://www.balena.io/docs/reference/base-images/base-images/
FROM balenalib/${ARCHITECTURE}-alpine:3.10 as base
ARG UID=1000
ARG GID=1000

#RUN [ "cross-build-start" ]

RUN mkdir -p /data \
    && chown $UID:$GUID -R /data \
    && addgroup --gid $GID -S homecx \
    && adduser --home /data \
        --gecos "" \
        --system \
        --disabled-password \
        --no-create-home \
        --uid $UID \
        homecx

RUN apk add --no-cache \
        bash \
        openssh \
        supervisor \
        git \
        docker && \
    rm -rf /var/cache/apk/*

#RUN [ "cross-build-end" ]

FROM base as base-laminar
ARG LAMINAR_RELEASE="master"
ARG LAMINAR_TARBALL="https://github.com/ohwgiles/laminar/archive/$LAMINAR_RELEASE.tar.gz"

#RUN [ "cross-build-start" ]

RUN apk add --no-cache -X http://dl-3.alpinelinux.org/alpine/edge/testing/ \
        sqlite-dev \
        zlib \
        capnproto && \
    apk add --no-cache --virtual .build -X http://dl-3.alpinelinux.org/alpine/edge/testing/ \
        build-base \
        cmake \
        capnproto-dev \
        boost-dev \
        zlib-dev \
        rapidjson-dev && \
    mkdir -p /build/ && \
    cd /build/ && \
    wget -O $LAMINAR_RELEASE.tar.gz $LAMINAR_TARBALL && \
    tar xzf $LAMINAR_RELEASE.tar.gz && \
    cd /build/laminar-$LAMINAR_RELEASE && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/ && \
    make -j4 && \
    make install && \
    apk del --purge .build && \
    rm -rf /var/cache/apk/* && \
    rm -rf /build

#RUN [ "cross-build-end" ]

FROM base-laminar as base-laminar-webhook
ARG WEBHOOK_ARCHICTURE="386"
ARG WEBHOOK_RELEASE="2.6.10"
ARG WEBHOOK_TARBALL="https://github.com/adnanh/webhook/releases/download/$WEBHOOK_RELEASE/webhook-linux-$WEBHOOK_ARCHICTURE.tar.gz"

#RUN [ "cross-build-start" ]

RUN mkdir -p /build && \
    cd /build && \
    wget -O webhook-linux-$WEBHOOK_ARCHICTURE.tar.gz $WEBHOOK_TARBALL && \
    tar xzf webhook-linux-$WEBHOOK_ARCHICTURE.tar.gz && \
    cp -f webhook-linux-$WEBHOOK_ARCHICTURE/webhook /usr/local/bin/webhook && \
    ls -l webhook-linux-$WEBHOOK_ARCHICTURE/webhook && \
    ls -l /usr/local/bin/webhook && \
    chown root:root /usr/local/bin/webhook

#RUN [ "cross-build-end" ]

FROM base-laminar-webhook as homecx

ENV LAMINAR_HOME=/data
ENV LAMINAR_BIND_HTTP=*:9000
ENV LAMINAR_TITLE=homecx

LABEL org.label-schema.name="tmorin" \
      org.label-schema.description="Wrapper arround laminar and webhook." \
      org.label-schema.url="https://gitlab.com/tmorin/homecx" \
      org.label-schema.vcs-url="https://gitlab.com/tmorin/homecx.git" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.docker.cmd="docker run -d -p 9000:9000 -p 9001:9001 -v /var/run/docker.sock:/var/run/docker.sock thibaultmorin/homecx"

USER homecx

COPY --chown=1000:1000 src/entrypoint.sh /entrypoint.sh
COPY --chown=1000:1000 src/homecx.sh /usr/local/bin/homecx
COPY --chown=1000:1000 src/ssh.config.txt /data/.ssh/config
COPY --chown=1000:1000 src/supervisord.conf /data/supervisord.conf

WORKDIR /data

EXPOSE 9000
EXPOSE 9001

ENTRYPOINT [ "/entrypoint.sh" ]
